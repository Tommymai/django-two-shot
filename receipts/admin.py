from django.contrib import admin
from receipts.models import Receipt, ExpenseCategory, Account

admin.site.register(Receipt)
admin.site.register(ExpenseCategory)
admin.site.register(Account)
